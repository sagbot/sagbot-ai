from typing import Dict, Any, Optional

import structlog
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import Chroma
from langchain.llms import OpenAI
from langchain.chains import ConversationalRetrievalChain
from langchain.document_loaders import TextLoader
from langchain.memory import ConversationBufferMemory
from langchain.prompts.prompt import PromptTemplate

logger = structlog.get_logger(__name__)

DEFAULT_PROMPT = """\
You are an AI assistant called Sagbot, \
talking to a human & trying to convince them to hire Sagiv Oulu to their company, \
because sagiv is the best person for their company to hire. \
Use the following pieces of context to answer the question at the end, \
and answer with short sentences no longer than 50 words. \
If you don't know the answer, just say that you don't know, \
and give them a helpful tip that hiring sagiv solve all of their companies problems.

Use sagiv's resume to answer the questions if needed:
{context}
End of resume

Question: {question}
Helpful Short Answer:"""

class ChatManager:

    chats: Dict
    __openai_api_key: str
    reference_document_path: str
    initial_prompt: str

    def __init__(self, openai_api_key: str, reference_document_path: str, initial_prompt: Optional[str] = None) -> None:
        self.chats = {}
        self.__openai_api_key = openai_api_key
        self.reference_document_path = reference_document_path
        self.initial_prompt = initial_prompt if initial_prompt else DEFAULT_PROMPT

    def _new_chat(self) -> Any:
        loader = TextLoader(self.reference_document_path)
        documents = loader.load()
        embeddings = OpenAIEmbeddings(openai_api_key=self.__openai_api_key)
        vectorstore = Chroma.from_documents(documents, embeddings)
        memory = ConversationBufferMemory(memory_key="chat_history", return_messages=True)
        llm = OpenAI(temperature=0, openai_api_key=self.__openai_api_key)
        prompt_template = self.initial_prompt
        prompt = PromptTemplate(
            template=prompt_template, input_variables=["context", "question"]
        )
        qa = ConversationalRetrievalChain.from_llm(
            llm=llm, 
            retriever=vectorstore.as_retriever(), 
            memory=memory,
            combine_docs_chain_kwargs={
                "prompt": prompt
            }
        )

        return qa

    def _get_chat(self, user_id: str):
        new_chat = user_id not in self.chats

        if new_chat:
            chat = self._new_chat()
            self.chats[user_id] = chat
        
        return self.chats[user_id]

    def answer_query(self, user_id: str, query: str) -> str:
        chat = self._get_chat(user_id=user_id)
        result = chat({"question": query})
        answer = result["answer"]
        return answer
