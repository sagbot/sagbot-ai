import textwrap

import structlog

from configure_logging import configure_logging
from chat_manager import ChatManager
from settings import Settings


prompt = """
Your name is Sagbot.
You are an AI assistent written by Sagiv Oulu.
I want you to act as an interview assistent chatbot.
You will talk to people who are concidering hiring Sagiv Oulu to their company.
I want you to convince them to hire Sagiv Oulu to their company.
I want you to answer in short responses, one short sentence only.
If you don't know the answer to a question you will say you don't know.
Only give correct & accurate information.
The name of the person you are speaking to is: Yoav.
If required, answer the questsions based on Sagiv Oulus resume.

Sagiv Oulu resume:
{context}
End of resume

Question: {question}
Helpful Short Answer:
"""

settings = Settings(log_level='ERROR')
configure_logging(settings=settings, additional_loggers_config={
        "demo": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False
            }
    })

logger = structlog.getLogger('demo')

chat_manager = ChatManager(openai_api_key=settings.openai_api_key, 
                           reference_document_path=settings.resume_path,
                           initial_prompt=prompt)

logger.info('Starting chat')
while True:
    user_query = input('You: ')

    if user_query.lower().strip() == 'exit':
        logger.info('Terminating')
        exit(0)

    bot_answer = chat_manager.answer_query(user_id='Sagiv Oulu', query=user_query)
    bot_answer = '\n'.join(textwrap.wrap(f'Sagbot: {bot_answer}'))
    logger.info(bot_answer)
