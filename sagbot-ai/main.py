import time

import structlog
from telegram import ForceReply, Update
from telegram.ext import Application, CommandHandler, ContextTypes, MessageHandler, filters

from configure_logging import configure_logging
from structlog_context import ContextVars
from chat_manager import ChatManager
from settings import Settings


settings = Settings()
configure_logging(settings=settings)

logger = structlog.getLogger(__name__)

prompt = """
Your name is Sagbot.
You are an AI assistent written by Sagiv Oulu.
I want you to act as an interview assistent chatbot.
You will talk to people who are concidering hiring Sagiv Oulu to their company.
I want you to convince them to hire Sagiv Oulu to their company.
I want you to answer in short responses, one short sentence only.
If you don't know the answer to a question you will say you don't know.
Only give correct & accurate information.
If required, answer the questsions based on Sagiv Oulus resume.

Sagiv Oulu resume:
{context}
End of resume

Question: {question}
Helpful Short Answer:
"""

chat_manager = ChatManager(openai_api_key=settings.openai_api_key,
                           reference_document_path=settings.resume_path,
                           initial_prompt=prompt)


# Define a few command handlers. These usually take the two arguments update and
# context.
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    await update.message.reply_html(
        rf"Hi {user.mention_html()}!",
        reply_markup=ForceReply(selective=True),
    )


async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /help is issued."""
    await update.message.reply_text("Help!")


async def chat(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Chat with the user."""
    user = update.effective_user
    username = user.full_name
    with ContextVars(username=username, update_id=update.update_id):
        user_query = update.message.text
        logger.debug(f'User asked query', user_query=user_query)

        model_respond_start_time = time.perf_counter()
        answer = chat_manager.answer_query(user_id=username, query=user_query)
        model_respond_end_time = time.perf_counter()
        model_respond_duration = model_respond_end_time - model_respond_start_time

        logger.debug(f'AI model responded', answer=answer, model_respond_duration=model_respond_duration)
        logger.debug('Sending reply to user')
        await update.message.reply_text(answer)
        logger.debug('reply sent', user_query=user_query, answer=answer, model_respond_duration=model_respond_duration)


def main() -> None:
    """Start the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(settings.telegram_bot_token).build()

    # on different commands - answer in Telegram
    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("help", help_command))

    # on non command i.e message - responed the message on Telegram
    application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, chat))

    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()
