from typing import Dict

from structlog.contextvars import get_contextvars, bind_contextvars, clear_contextvars


class ContextVars:

    __original_context_vars: Dict

    def __init__(self, **kwargs):
        self.__original_context_vars = {}
        self.__context_vars = kwargs

    def __enter__(self):
        self.__original_context_vars = get_contextvars()
        bind_contextvars(**self.__context_vars)

    def __exit__(self, exc_type, exc_val, exc_tb):
        clear_contextvars()
        bind_contextvars(**self.__original_context_vars)