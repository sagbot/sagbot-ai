# Sagiv Oulu
Dedicated, autodidact & autonomous worker with great people skills. Looking for work as a backend developer / devops engineer 

## Contact information
* email address: sagiv.oulu@gmail.com
* linkedin account: https://www.linkedin.com/in/sagiv-oulu-175476111/


## Work Experience
### IDF Unit 8200 — Software engineer + DevOps engineer
Work duration: August  2020 - November 2022

Developed automations & infrastructure for a private cloud system for unit 8200.
* Designed A security system for authentication & authorization of users within tight multi-network security constraints 
* Implemented splunk monitoring infrastructure for early detection of bugs & errors + BI analysis of system usage over many microservices
* Wrote a generic helm chart & re-deployed our Kubernetes microservices using it
* Implemented Gitlab CI/CD Pipeline for deploying microservices that includes: Building, Linting, Testing, Deploying & publishing packages

In addition, during the Covid pandemic I worked for the Isrealy Ministry of Health in Covid data analysis, with the focus of monitoring infections in the elderly & alerting the medical staff.


### IDF Unit Sapir — Software engineer + DevOps engineer
Work duration: January 2018 - August 2020

Developed various automations for the Sapir unit, that is responsible for the infrastructure of the IDF Intelligence corp.
* Deployed Cloud infra in Azure using Terraform auto-deployment, for dedicated IDF systems
* Developed & maintained  a private cloud for the Intelligence corp for deploying & management of on-prem infra
* Developed firewall rule optimization software to reduce load on Checkpoint on prem firewall
* Developed a tool for redacting classified information (ips, server names, passwords, data centers etc… ) from logs & files before sending them to private support companies
* Developed a system for managing & making video calls / conferences

## Education
* Open University — Computer Science Courses
Completed mutiple courses in the Open University for a Computer Science degree
* IDF Basmah course — Programming course (Tohnitan course). IDF Computer programming course that trains, prepares & sends soldiers to every branch of the army.
* Ort College — Software Technician. A computer programming certificate in C, Java, SQL & assembly, In preparation for military service as a programmer.
* High school FRC Robotics group — Team captain. Founded & led our high school robotics team in the international FRC competition. This included financial management, student recruitment, programming robots, 3D modeling & manufacturing parts.
* Mekif Yahud High school — High school diploma. Bagrut certificate with 10 pt programming, 5 pt physics, 5 pt english, 5 pt math

## Personal projects
* Sagbot — telegram chatbot powered by ChatGPT
  An AI Powered chatbot that is preloaded with knowledge of my work experience via my resume. Feel free to ask Sagbot about me! web.telegram.org/k/#@ask_sagbot
* Structlog-demo — Python FastAPI Server with advanced logging
  A demo project of how to run a python fastapi server with detailed logging that helps to investigate bugs & gather monitoring data.
  https://gitlab.com/sagbot/structlog-demo
* Sagbot-aks — Azure kubernetes deployment using terraform & gitlab ci
  https://gitlab.com/sagbot/sagbot-aks
* helm-repo — Helm charts repository
  A helm chart repo for custom charts that I developed, along side a Gitlab CI pipeline that publishes the charts as a helm chart repository using Gitlab pages
  https://gitlab.com/sagbot/helm-repo
* FluentD-Monitoring — An http endpoint monitoring tool
  A tool that uses fluentd in order to monitor an HTTP endpoint, and send the resulting information to a database.
  https://gitlab.com/sagbot/fluentd-monitoring

## Skills
### Programming languages
Python, Java, Javascript, Typescript, C#, C, SQL, HTML, CSS, Lua.

### Libraries & frameworks
FastAPI, Celery, NodeJS, Django, React, AngularJs, Locust.

### IT Products
Gitlab CI, Nginx, Keycloak (Red Hat SSO), vRA, vCenter, LTM, MongoDB, Elastic, Kibana, Splunk, FluentD Docker, docker-compose, Helm, Ansible, Terraform.

### Protocols
SAML (Authentication protocol), OpenID Connect

## Awards
* Independence day excellence award 2019 Sapir unit IDF.
* Rosh hashanah excellence award 2022 unit 8200 IDF.

## Languages
* Hebrew - native speaker.
* English - native speaker.

## Personal life information
* I was born on 14th of march 1998 in Israel.
* I served in the IDF for 6 years in various computer engineering positions in the Intelligance corp & unit 8200 specifically.
* After finishing my military service I traveled asia for 3 months, spending most of my time in Viatnam and Thailand.
* I have many personal hobbies, some of which include:
    * Gardening
    * Riding a bicycle
    * Kayaking in the sea of galile
    * Playing the guitar
    * Making coffee on my home barista machine
