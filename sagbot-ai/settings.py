from pydantic import BaseSettings, DirectoryPath

class Settings(BaseSettings):
    telegram_bot_token: str
    openai_api_key: str
    resume_path: str = './sagbot-ai/knowledge_base/sagiv_cv.md'

    logs_dir: DirectoryPath = '/var/log'
    log_level: str = 'DEBUG'

    console_color_logs: bool = True
    console_json_logs: bool = False

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'
