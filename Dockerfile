FROM python:3.10.11-bullseye

WORKDIR /app

# Copy the requirements file & install it
COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

# Give permissive permissions to the chroma dir, that the code uses to process text 
#   files (when running in k8s somtimes the container runs with a different user)
RUN mkdir .chroma && chmod 777 .chroma && chmod -R 777 /var/log

# Copy the code into the image
COPY . .

# Run the application
CMD ["python3", "sagbot-ai/main.py"]